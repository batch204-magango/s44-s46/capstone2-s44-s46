const mongoose = require("mongoose");

const productSchema = new mongoose.Schema ({
	userId:{
		type: String,
		required: [true, "UserId is required"]
	},
	products: [{
		productId: {
			type:String,
			required: [true, "ProductId is required"]
		},
		quantity:{
			type:Number,
			default: 1,
		}

	}],
	totalAmount:{
		type:Number,
		required: [true, "Total amount is required"]
	},

	purchasedOn:{
		timestamps:true

	}


});




module.exports = mongoose.model("Order", productSchema);