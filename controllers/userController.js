const User = require("../models/User");
const Course = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");


//Controller for User Registration
//new registration
module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result === null){

			if(reqBody.password.length >= 8){
				let newUser = new User({
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				});

				return newUser.save().then((user, error) => {

					if(error) {
						return false
					} else {
						return 'Registered Successfully'
					}
				})
			}
			else{
				return `Password is too weak, try another one`
			}		
		}
		else{
			return `User is already registered`
		}
	})
}



//-------------
// module.exports.registerUser = (reqBody) => {
// 	let newUser = new User ({
// 		firstName: reqBody.firstName,
// 		lastName: reqBody.lastName,
// 		email: reqBody.email,
// 		mobileNo: reqBody.mobileNo,
// 		password: bcrypt.hashSync(reqBody.password, 10)
// 	})



// 	return newUser.save().then((user, error)=> {

// 		if(error) {
// 			return false
// 		} else {
// 			return 'Registered Successfully'
// 		}
// 	})
// }


//User Authentication
module.exports.loginUser = (reqBody)=> {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				console.log(result)
				return {
					access:auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//Retrieve user details
module.exports.getUser = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return result
	})
}
